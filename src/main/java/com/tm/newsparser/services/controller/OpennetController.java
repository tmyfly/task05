package com.tm.newsparser.services.controller;

import com.tm.newsparser.app.NewsCounterUcsOpenImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/opennet")

public class OpennetController {
    @Autowired
    private NewsCounterUcsOpenImpl newsCounterUcs;

    @GetMapping
    public String opennetPage(Model model){
        List<String> listHead = newsCounterUcs.run();
        model.addAttribute("listHead", listHead);
        return "opennet";
    }

}
