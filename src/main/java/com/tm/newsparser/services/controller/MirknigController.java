package com.tm.newsparser.services.controller;

import com.tm.newsparser.app.NewsCounterUcsMirknigImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/mirknig")
public class MirknigController {
    @Autowired
    private NewsCounterUcsMirknigImpl newsCounterUcsMirknig;

    @GetMapping
    public String mirknigPage(Model model){
        List<String> listHead = newsCounterUcsMirknig.run();
        model.addAttribute("listHead", listHead);
        return "mirknig";
    }

}
