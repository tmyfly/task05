package com.tm.newsparser.app;

import com.tm.newsparser.domain.count.NewsCounterService;
import com.tm.newsparser.domain.count.ParsHead;
import com.tm.newsparser.domain.download.WebPageDownloadService;
import com.tm.newsparser.domain.printing.NewsPrintingService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class NewsCounterUcsMirknigImpl implements NewsCounterUcs {

    private final WebPageDownloadService webPageDownloadService;
    private final NewsCounterService newsCounterService;
    private final NewsPrintingService newsPrintingService;
    private final ParsHead mirknigParsHead;

    public NewsCounterUcsMirknigImpl(@Qualifier("wpd2") final WebPageDownloadService webPageDownloadService,
                                     @Qualifier("ncs2") final NewsCounterService newsCounterService,
                                     @Qualifier("nps2") final NewsPrintingService newsPrintingService,
                                     @Qualifier("ph2") final ParsHead mirknigParsHead) {
        this.webPageDownloadService = webPageDownloadService;
        this.newsCounterService = newsCounterService;
        this.newsPrintingService = newsPrintingService;
        this.mirknigParsHead = mirknigParsHead;
    }

    public List<String> run(){
        final String page = webPageDownloadService.download();
        final int result = newsCounterService.count(page);
        return mirknigParsHead.returnHeadList(page,result);
    }
}
