package com.tm.newsparser.app;

import com.tm.newsparser.domain.count.NewsCounterService;
import com.tm.newsparser.domain.count.ParsHead;
import com.tm.newsparser.domain.download.WebPageDownloadService;
import com.tm.newsparser.domain.printing.NewsPrintingService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class NewsCounterUcsOpenImpl implements NewsCounterUcs {

    private final WebPageDownloadService webPageDownloadService;
    private final NewsCounterService newsCounterService;
    private final NewsPrintingService newsPrintingService;
    private final ParsHead openParsHead;

    public NewsCounterUcsOpenImpl(@Qualifier("wpd1") final WebPageDownloadService webPageDownloadService,
                                  @Qualifier("ncs1") final NewsCounterService newsCounterService,
                                  @Qualifier("nps1") final NewsPrintingService newsPrintingService,
                                  @Qualifier("ph1") final ParsHead openParsHead) {
        this.webPageDownloadService = webPageDownloadService;
        this.newsCounterService = newsCounterService;
        this.newsPrintingService = newsPrintingService;
        this.openParsHead = openParsHead;
    }

    public List<String> run(){
        final String page = webPageDownloadService.download();
        final int result = newsCounterService.count(page);
        return openParsHead.returnHeadList(page,result);
    }
}
