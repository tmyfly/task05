package com.tm.newsparser.app;

import com.tm.newsparser.domain.count.*;
import com.tm.newsparser.domain.count.*;
import com.tm.newsparser.domain.download.OpenDownloadServiceImpl;
import com.tm.newsparser.domain.download.WebPageDownloadService;
import com.tm.newsparser.domain.printing.NewsPrintingService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:application-opennet.properties")
public class OpenAppConfiguration {
    private final String urlOpen;
    private final String searchPatternOpen;
    private final String searchPatternHeadOpen;

    public OpenAppConfiguration( @Value("${urlOpen}") final String urlOpen,
                                 @Value("${searchPatternOpen}") final String searchPatternOpen,
                                 @Value("${searchPatternHeadOpen}") final String searchPatternHeadOpen) {
        this.urlOpen = urlOpen;
        this.searchPatternOpen = searchPatternOpen;
        this.searchPatternHeadOpen = searchPatternHeadOpen;
    }

    @Bean("rc1")
    ReturnContent returnContent(){
        return new ReturnContentImpl();
    }

    @Bean("wpd1")
    WebPageDownloadService webPageDownloadService() {
       return new OpenDownloadServiceImpl(urlOpen, returnContent());
    }

    @Bean("ncs1")
    NewsCounterService newsCounterService(){
        return new OpennetCounterServiceImpl(searchPatternOpen);
    }

    @Bean("nps1")
    NewsPrintingService newsPrintingService(){
        return new NewsPrintingService(urlOpen);
    }

    @Bean("ph1")
    ParsHead parsHead(){
        return new OpenParsHead(searchPatternHeadOpen);
    }
}