package com.tm.newsparser.app;

import com.tm.newsparser.domain.count.*;
import com.tm.newsparser.domain.download.MirknigDownloadServiceImpl;
import com.tm.newsparser.domain.download.WebPageDownloadService;
import com.tm.newsparser.domain.printing.NewsPrintingService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:application-mirknig.properties")
public class MirknigiAppConfiguration {
    private final String urlMir;
    private final String searchPatternMir;
    private final String searchPatternHeadMir;
    private final String searchPatternHeadTarget1;
    private final String searchPatternHeadTarget2;

    public MirknigiAppConfiguration(@Value("${urlMir}") final String urlMir,
                                    @Value("${searchPatternMir}") final String searchPatternMir,
                                    @Value("${searchPatternHeadMir}") final String searchPatternHeadMir,
                                    @Value("${searchPatternHeadTarget1}") final String searchPatternHeadTarget1,
                                    @Value("${searchPatternHeadTarget2}") final String searchPatternHeadTarget2) {
        this.urlMir = urlMir;
        this.searchPatternMir = searchPatternMir;
        this.searchPatternHeadMir = searchPatternHeadMir;
        this.searchPatternHeadTarget1 = searchPatternHeadTarget1;
        this.searchPatternHeadTarget2 = searchPatternHeadTarget2;
    }

    @Bean("rc2")
    ReturnContent returnContent(){
        return new ReturnContentImpl();
    }

    @Bean("wpd2")
    WebPageDownloadService webPageDownloadService() {
       return new MirknigDownloadServiceImpl(urlMir, returnContent());
    }

    @Bean("ncs2")
    NewsCounterService newsCounterService(){
        return new MirknigCounterServiceImpl(searchPatternMir);
    }

    @Bean("nps2")
    NewsPrintingService newsPrintingService(){
        return new NewsPrintingService(urlMir);
    }

    @Bean("ph2")
    ParsHead parsHead(){
        return new MirknigParsHead(searchPatternHeadMir, searchPatternHeadTarget1, searchPatternHeadTarget2);
    }
}