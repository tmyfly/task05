package com.tm.newsparser.app;

import java.util.List;

public interface NewsCounterUcs {
    public List<String> run();
}
