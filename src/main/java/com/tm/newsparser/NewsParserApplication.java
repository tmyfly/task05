package com.tm.newsparser;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NewsParserApplication {

	public static void main(String[] args) {
		SpringApplication.run(NewsParserApplication.class, args);
	}

}
