package com.tm.newsparser.domain.count;


import org.springframework.beans.factory.annotation.Qualifier;

public class MirknigCounterServiceImpl extends AbstractCounterService{

    public MirknigCounterServiceImpl(@Qualifier("searchPatternMir")final String searchPattern) {
        super(searchPattern);
    }
}
