package com.tm.newsparser.domain.count;

public interface ReturnContent {
    public String contentReturn(final String url);
}
