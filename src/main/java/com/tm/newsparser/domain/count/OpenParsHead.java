package com.tm.newsparser.domain.count;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class OpenParsHead implements ParsHead{
    private final String searchPatternHead;

    public OpenParsHead(final String searchPattern) {
        this.searchPatternHead = searchPattern;
    }

    @Override
    public List<String> returnHeadList(final String page, final int count) {

        final Pattern pattern = Pattern.compile(searchPatternHead);
        final Matcher matcher = pattern.matcher(page);
        final List<String> list = new LinkedList<>();
        while (matcher.find()) {
            list.add(matcher.group(1));
        }

        return IntStream.range(0, count)
                .mapToObj(list::get)
                .collect(Collectors.toCollection(LinkedList::new));
    }
}
