package com.tm.newsparser.domain.count;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class AbstractCounterService implements NewsCounterService{

    private final String searchPattern;

    public AbstractCounterService(final String searchPattern) {
        this.searchPattern = searchPattern;
    }

    public int count(final String strPars){
        int counts = 0;

        final Pattern pattern = Pattern.compile(this.searchPattern);
        final Matcher matcher = pattern.matcher(strPars);
        while (matcher.find()) {
            counts++;
        }
        return counts;
    }
}
