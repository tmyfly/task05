package com.tm.newsparser.domain.count;

public interface NewsCounterService {
    public int count(final String str);
}
