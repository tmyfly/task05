package com.tm.newsparser.domain.count;

import org.apache.hc.client5.http.classic.methods.HttpGet;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.CloseableHttpResponse;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.core5.http.ParseException;
import org.apache.hc.core5.http.io.entity.EntityUtils;

import java.io.IOException;

public class ReturnContentImpl implements ReturnContent{
    @Override
    public String contentReturn(final String url) {
        String contentPage="";
        try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
            final HttpGet httpGet = new HttpGet(url);
            httpGet.setHeader("Accept", "application/xml");
            httpGet.setHeader("User-Agent",
                    "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) " +
                            "Chrome/23.0.1271.95 Safari/537.11");

            try (final CloseableHttpResponse response = httpclient.execute(httpGet)) {
                contentPage = EntityUtils.toString(response.getEntity());
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return contentPage;
    }
}
