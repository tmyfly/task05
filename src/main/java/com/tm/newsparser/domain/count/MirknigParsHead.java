package com.tm.newsparser.domain.count;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class MirknigParsHead implements ParsHead{
    private final String searchPatternHead;
    private final String searchPatternHeadTarget1;
    private final String searchPatternHeadTarget2;

    public MirknigParsHead(final String searchPatternHead,
                           final String searchPatternHeadTarget1,
                           final String searchPatternHeadTarget2) {
        this.searchPatternHead = searchPatternHead;
        this.searchPatternHeadTarget1 = searchPatternHeadTarget1;
        this.searchPatternHeadTarget2 = searchPatternHeadTarget2;
    }

    @Override
    public List<String> returnHeadList(final String page, final int count) {
        final List<String> list = new LinkedList<>();

        final Pattern pattern = Pattern.compile(searchPatternHead);
        final Matcher matcher = pattern.matcher(page);
        while (matcher.find()) {
            list.add(matcher.group(1));
        }

        return IntStream.range(0, count)
                .mapToObj(list::get)
                .map(temp -> temp.trim().replace(searchPatternHeadTarget1, ""))
                .map(temp -> temp.trim().replace(searchPatternHeadTarget2, ""))
                .collect(Collectors.toCollection(LinkedList::new));
    }
}
