package com.tm.newsparser.domain.count;

import java.util.List;

public interface ParsHead {
    public List<String> returnHeadList(final String page, final int count);
}
