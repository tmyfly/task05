package com.tm.newsparser.domain.download;

import com.tm.newsparser.domain.count.ReturnContent;
import org.springframework.beans.factory.annotation.Qualifier;

public class OpenDownloadServiceImpl extends AbstractDownloadService{

    public OpenDownloadServiceImpl(final String url, @Qualifier("rc1") final ReturnContent returnContent) {
        super(url, returnContent);
    }
}
