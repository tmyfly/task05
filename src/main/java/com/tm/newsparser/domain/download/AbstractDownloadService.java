package com.tm.newsparser.domain.download;

import com.tm.newsparser.domain.count.ReturnContent;

public abstract class AbstractDownloadService implements WebPageDownloadService{

    private final String url;
    private final ReturnContent returnContent;

    public AbstractDownloadService(final String url, final ReturnContent returnContent) {
        this.url = url;
        this.returnContent = returnContent;
    }

    @Override
    public String download() {
        final String contentPage;
        contentPage = returnContent.contentReturn(this.url);
        return contentPage;
    }
}
