package com.tm.newsparser.domain.download;

public interface WebPageDownloadService {
    public String download();
}
