package com.tm.newsparser.domain.download;

import com.tm.newsparser.domain.count.ReturnContent;
import org.springframework.beans.factory.annotation.Qualifier;

public class MirknigDownloadServiceImpl extends AbstractDownloadService{
    public MirknigDownloadServiceImpl(final String url, @Qualifier("rc2") final ReturnContent returnContent) {
        super(url, returnContent);
    }
}
