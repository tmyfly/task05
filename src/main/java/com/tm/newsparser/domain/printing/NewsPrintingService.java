package com.tm.newsparser.domain.printing;


public class NewsPrintingService {
    private final String url;

    public NewsPrintingService(final String url) {
        this.url = url;
    }

    public void printNewsCount(final int result){
        System.out.println("The amount of today's news on "+url+" "+"is "+result);
    }
}
